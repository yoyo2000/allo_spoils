## Agrégation de critiques spectateur de films d'allocine comportant des spoilers.

Usage :
```shell
python3 allo_spoils.py 218395 19776 27063
```

Les arguments sont les ID allocine de films que l'on peut obtenir à partir de l'URL d'une fiche.
Les arguments peuvent être omis si un ID est copié dans le presse-papier.

Génère une page html par film dans le dossier results.

Testé sur Ubuntu et Windows.

## Installation

La bibliothèque [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup) est nécessaire.

Pour l'installer sous Debian/Ubuntu :
```shell
sudo apt install python3-bs4
```

Une autre bibliothèque facultative, [pyperclip](https://pypi.org/project/pyperclip/) peut être installée pour la gestion du presse-papier.

Sous Debian/Ubuntu :
```shell
sudo apt install python3-pyperclip
```


## License

WTFPL : http://www.wtfpl.net/