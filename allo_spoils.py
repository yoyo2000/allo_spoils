#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
import bs4
import webbrowser
import re
import unicodedata
from urllib.request import urlopen
from urllib.error import HTTPError

HELP = """
Agrégation de critiques spectateur de films d'allocine comportant des spoilers.

Usage : python3 allo_spoils.py 218395 19776 27063

Les arguments sont les ID allocine de films que l'on peut obtenir à partir de l'URL d'une fiche.
"""

URL_FICHE = "http://www.allocine.fr/film/fichefilm_gen_cfilm={0}.html"
URL_SPEC = "http://www.allocine.fr/film/fichefilm-{0}/critiques/spectateurs"
RESULT_DIR = os.path.join(os.getcwd(), "results")

def get_url(allocine_film_id):
    return URL_SPEC.format(allocine_film_id)

def get_infos(page):
    infos = {}
    titre = str(page.find("meta", {"property" : "og:title"}).get("content"))[17:]
    if titre == "":
        #Pas de page critiques spectateur donc redirection sur la fiche du film
        infos["titre"] = str(page.find("meta", {"property" : "og:title"}).get("content"))
        infos["total"] = "0"
        return infos
    infos["titre"] = titre
    infos["total"] = page.find("h2", {"class" : "titlebar-title titlebar-title-md"}).string.split(" ")[0].replace("\u202f", "")
    infos["star-5"] = page.find_all("span", {"class" : "nb"})[0].string.split(" ")[0]
    infos["star-4"] = page.find_all("span", {"class" : "nb"})[1].string.split(" ")[0]
    infos["star-3"] = page.find_all("span", {"class" : "nb"})[2].string.split(" ")[0]
    infos["star-2"] = page.find_all("span", {"class" : "nb"})[3].string.split(" ")[0]
    infos["star-1"] = page.find_all("span", {"class" : "nb"})[4].string.split(" ")[0]
    infos["star-0"] = page.find_all("span", {"class" : "nb"})[5].string.split(" ")[0]
    return infos

def get_com_spoiled(page):
    for com in page.findAll("div", {"class" : "content-txt review-card-content"}):
        if len(com.findAll("span", {"class" : "spoiler-container"})) > 0:
            for spoil in com.findAll("span", {"class" : "spoiler-container"}):
                try:
                    temp = spoil.find("span", {"class" : "spoiler-content"}).string
                    spoil.name = "mark"
                    del spoil["class"]
                    del spoil["onclick"]
                    spoil.clear()
                    spoil.string = temp
                except:
                    continue
            com.name = "article"
            #del com["itemprop"]
            n = page.new_tag("header")
            note = com.find_previous_sibling("div", {"class":"review-card-meta"}).find("span", {"class":"stareval-note"}).string.strip()
            #date_pub = com.find_previous_sibling("div", {"class":"review-card-meta"}).find("span", {"class":"review-card-meta-date light"}).string.strip()
            n.string = f"Note : {note}"     #"{0} - {1}".format(date_pub, note)
            com.insert(0, n)
            # FIXME : point sensible ici, les 2 lignes suivantes font planter plus loin lors du "sortie.find(id="infos").append"
            #~ com.contents[1] = com.contents[1].strip()
            #~ com.contents[-1] = com.contents[-1].strip()
            note_allo = str(int(float(note.replace(",","."))))
            if infos.get("nb-"+note_allo) is None:
                infos["nb-"+note_allo] = 1
            else:
                infos["nb-"+note_allo] += 1
            spoils["spoils"].append(com)    #Ajout au dico "spoils" déclaré dans le __main__

def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to hyphens.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub(r'[^\w\s-]', '', value.decode('utf-8')).strip()
    return value

def byebye():
    print(HELP)
    sys.exit()

if __name__ == '__main__':
    ids = sys.argv[1:]
    if len(ids)==0:
        try:
            import pyperclip
            clip = pyperclip.paste()
            p = re.compile(r'\d+')
            if p.fullmatch(clip):
                ids = [clip]
            else:
                byebye()
        except:
            byebye()
    
    if not os.path.isdir(RESULT_DIR):
        os.mkdir(RESULT_DIR)
        
    html = """<html><head><meta charset='UTF-8'><style>
           body {
                font-family: sans-serif;
            }
           h1 {
                text-align: center;
                margin-top: 30px;
            }
            #main {
                width: 50%;
                margin-left: 450px;
            }
            #main aside {
                display: flex;
                /*! margin: 0em 0em; */
                padding: 0.5em;
            }
            #main aside p {
                border: 1px dashed #ccc;
                padding: 5px;
                background: #ddd;
            }
            article {
                border:1px dashed #444;
                margin:1em;
                padding:1em;
                background:#fff;
            }
            article aside {
                background:#fff;
                margin: 0em 0em;
                padding: 0.5em;
                border-bottom: 1px dashed #ccc;
            }
            article header, article footer {
                border: 1px dashed #ccc;
                padding: 1em;
                background: #ddd;
            }
            </style></head><body><h1 id='titre'></h1><section id='main'><aside id="infos"></aside></section></body></html>
    """
    for j in range(0, len(ids)):
        try:
            htmlfile = urlopen(get_url(ids[j])).read()
        except HTTPError:
            print("*** ID={0} : Page introuvable ({1}).".format(ids[j], get_url(ids[j])))
            continue
        page = bs4.BeautifulSoup(htmlfile, "lxml")

        spoils = {"infos": [], "spoils": []}
        sortie = bs4.BeautifulSoup(html, "lxml")
        infos = get_infos(page)
        if infos["total"] == "0":
            #Aucune critique trouvée : on passe au film suivant
            print("*** Film : {0}".format(infos["titre"]))
            print("Aucune critique trouvée.")
            continue
        
        max_page = int(int(infos["total"])/15)+1
        print("*** Film : {0}".format(infos["titre"]))
        print("Lecture des {0} commentaires trouvés ({1} pages).".format(infos["total"], max_page))
        print("{0}/{1}".format(1, max_page), end="\r")
        get_com_spoiled(page)   #Appel pour la première page
        for i in range(2, max_page+1):
            print("{0}/{1}".format(i, max_page), end="\r")
            url = get_url(ids[j])+"/?page="+str(i)
            page = bs4.BeautifulSoup(urlopen(url).read(), "lxml")
            get_com_spoiled(page)
        sortie.find(id="titre").string = infos["titre"]
        if len(spoils["spoils"]) == 0:
            print("Aucun spoiler trouvé.")
            continue
        # Parser bs4 = "html.parser" sinon il insert aussi "<html><head></head><body>...</body></html>"
        stat = "<p><a href='{0}'>Fiche du film</a></p>".format(URL_FICHE.format(ids[j]))
        stat += "<p><b>Total :</b> {0} spoils sur {1} commentaires</p>".format(len(spoils["spoils"]), infos["total"])
        stat += "<p><b>5 Étoiles :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-5", "0"), infos["star-5"])
        stat += "<p><b>4 Étoiles :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-4", "0"), infos["star-4"])
        stat += "<p><b>3 Étoiles :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-3", "0"), infos["star-3"])
        stat += "<p><b>2 Étoiles :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-2", "0"), infos["star-2"])
        stat += "<p><b>1 Étoile  :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-1", "0"), infos["star-1"])
        stat += "<p><b>0 Étoile  :</b> {0} spoils sur {1} commentaires</p>".format(infos.get("nb-0", "0"), infos["star-0"])
        sortie.find(id="infos").append(bs4.BeautifulSoup(stat, "html.parser"))
        for num, div in enumerate(spoils["spoils"]):
            div.header.string.insert_before("#"+str(num+1))
            sortie.find(id="main").append(div)
        
        file_abspath = os.path.join(RESULT_DIR, slugify(infos["titre"]))+".html"
        with open(file_abspath, "wb") as f:
            f.write(sortie.prettify("utf-8"))
            f.flush()
            webbrowser.open(f.name, new=2)
